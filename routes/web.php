<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//nose cambiaron las rutas
Route::get('/', 'ViewsController@login');
Route::get('/registro', 'ViewsController@register');
Route::get('/succes', 'ViewsController@verif');
Route::get('/pass', 'ViewsController@pass');
Route::get('/error', 'ViewsController@error');
Route::get('/mantenimiento', 'ViewsController@mantenimiento');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/home', 'HomeController@index')->name('home');

// E-mail verification
Route::get('/emails/{code}', 'GuessController@verify');

//Route::get('image-upload',['as'=>'image.upload','uses'=>'RegisterControllerr@imageUpload']);
