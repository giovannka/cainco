<?php

namespace App\Http\Controllers;
use Illuminate\Notifications\Notification;
use App\User;
use Illuminate\Http\Request;

class GuessController extends Controller
{
    public function verify($code)
    {
        $user = User::where('confirmation_code', $code)->first();
    
        if (! $user)
            return redirect('/error');
    
        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();
    
        return redirect('/pass')->with('notification', 'Has confirmado correctamente tu correo!');
    }
}
