<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use \Input as Input;
//use Auth\RegisterControler;

class ViewsController extends Controller
{
    public function home () {
        return view('home');
    }
    public function register () {
        $cities = [
          'Opción no seleccionada','La Paz', 'Trinidad', 'Sucre', 'Cochabamba', 'Oruro', 'Cobija', 'Potosí', 'Santa Cruz de la Sierra', 'Tarija'
        ];
        $countries = [
          'Opción no seleccionada','Bolivia', 'Argentina', 'Brazil', 'Peru','Venezuela','Chile','Ecuador','Cuba','Mexico','Alemania','Canada','Uruguay','Paraguay','China','Japon','Corea','Italia','Estados Unidos','Francia','Inglaterra'
        ];
        $companyTypes = [
          'Opción no seleccionada','Comercial', 'Industria', 'Servicios',
        ];
        //se agregaron los campos de actividad y servicio
        $actividad = [
          'Opción no seleccionada','Agricola,Pecuario y Hortofruticola', 'Industria Grafica', 'Maquinaria, equipos e implementacion para agricultura', 'Industria Papelera','Alimentos y Bebidas','Maquinarias, implementos y equipos para la industris Alimenticia','Maquinarias, implementos y equipos para la industria de la bebida','Equipos de transporte y repuestos','Construccion y sus productos','Equipos e implementos Hidraulicos','Plasticos Afines','Electrodomesticos y linea blanca','Quimicos','Artesanias','Computacion,comunicacion y telefonia','Envases, embalajes y afines','Maquinaria, equipo e implementacion para la industria Quimica','Orfebreria,joyeria y bisuteria','Farmacias,medicamentos(productos medicos yhospitalarios)','Perfumeria y cosmeticos','Equipos electricos y electronicos','Metal mecanica y electromecanica','Mineria','Equipos e implementos petroleros','Maquinas, equipos e implementos para mineria','Productos derivados del petroleo','Industria de la madera','Equipos de seguridad','Muebles en general/furniture','Articulos de limpieza y para el hogar','Maquinaria.equipos e implementos para la industria de la madera','Jugueteria y articulos de entretenimiento','Industria textil y confecciones','Energia','Pieles, cuero, calzados y manufacturas de cuero','Ferreteria y herramientas','Maquinaria para la industria de pieles, cuero, calzados y manufacturas de cuero','otros',          
        ];
        $servicio = [
          'Opción no seleccionada','Empresa Comercial', 'Turismo, Hoteleria y Restaurante', 'Bancos y Financieras', 'Servicios Empresariales','Fletes y transporte','Servicios de construccion e ingenieria','Servicios Aduaneros','Servicios Informaticos y de Telecomunicacion','Almacenamiento y Distribucion','Servicios para la Agroindustria','Seguros','Otros Servicios',          
        ];
        return view('register', [
          'countries' => $countries,
          'cities' => $cities,
          'companyTypes' => $companyTypes,
          'actividad' => $actividad,
          'servicio' => $servicio
        ]);
    }
    public function login () { 
      notify('Welcome back!', 'success'); 
      return view('welcome');
    }

    public function upload(){
      
          if(Input::hasFile('logo')){
      
            echo 'Uploaded';
            $file = Input::file('logo');
            $file->move('uploads', $file->getClientOriginalName());
            echo '';
          }
      
    }

 public function verif () {  
      return view('/succes');
    }

    public function pass () {  
      return view('/pass');
    }

    public function error () {  
      return view('/error');
    }

    public function mantenimiento () {  
      return view('/mantenimiento');
    }
}
