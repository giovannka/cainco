<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use Mail;
//se agrego el modelo "Actserv" que permite la relacion con otra tabla donde se ingresan los datos de actividad y servicios
use App\Actserv;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Webcraft\Random\RandomFacade;
use App\Jobs\SendVerificationEmail;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    
   
    protected $redirectTo = '/succes';

   /*protected function redirectTo()
    {
        //$email = $data['email'];
        //return View::Make()->with('email',$email);
        return '/succes';
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      //se validaron los campos de acuerdo al tipo de entrada en el formulario
      //los que inician con "nullable" son aquellos campos opcionales
      //los "require" son aquellos campos necesarios
      //"actv" y "srv" son los arrays pertenecientes a los campos actividad y servicio
      //debe tenerse cuidad con la insercion de nuevos campos previamente debe verificarse si son requeridos "require" u opcioanles "nullable"  
        return Validator::make($data, [
            //'name' => 'required|string|max:255',
            'email' => 'required|string|email|min:6|max:128|unique:users',
            'password' => 'required|string||min:6|max:128|confirmed',
            'nombre_empresa' => 'required|string|unique:users|min:1|max:255',
            'pais' => 'required|string|not_in:Opción no seleccionada|max:128',
            'tipo' => 'required|string|not_in:Opción no seleccionada|max:128',
            'ciudad' => 'required|string|min:1|max:200',
            'direccion' => 'required|string|min:1|max:255',
            'telefono' => 'nullable|string',
            'celular_e' => 'required|unique:users|numeric',
            'postal' => 'nullable|string|min:1|max:128',
            'nit' => 'nullable|unique:users|numeric|min:1',
            'web' => 'nullable|string|min:1|max:255',
            'facebook' => 'nullable|string|min:1|max:255',
            'oferta' => 'required|string|min:1|max:255',
            'demanda' => 'required|string|min:1|max:255',
            'nombre_r' => 'required|string|min:1|max:128',
            'ci' => 'required|unique:users|numeric',
            'email_r' => 'required|unique:users|string|email|min:1|max:128',
            'cargo' => 'required|string|min:1|max:128',
            'celular_r' => 'required|numeric|unique:users',
            //'logo' => 'nullable|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'actv'  => 'required|array|min:1',
            'actv.0'  => 'required|string|not_in:Opción no seleccionada|max:255',
            'actv.1'  => 'string|max:255',
            'actv.2'  => 'string|max:255',
        
            'srv' => 'required|array|min:1',
            'srv.0' => 'required|string|not_in:Opción no seleccionada|max:255',
            'srv.1' => 'string|max:255',
            'srv.2' => 'string|max:255', 
        ]);    
    }
        
                            
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

     //una vez los campos del formularion fueron validados (funcion validator arriba) se porcede con el llenado a la BD
     //la siguiente funcion (create) cumple con ese objetivo
     //el formato de asignacion de campos es: campo_db => valor
     //la variable "$data" es un array que conetiene todos los campos del formula
     //la funcion "create" introduce datos en una tabla en eeste caso en la tabla "users"
    protected function create(array $data)
    {
        //se introduce datos en la tabla users
        //la variable $users corresponde a la tabla users y por lo tanto representa a una empresa 
        $cod = RandomFacade::generateString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'); //se genera un unico codigo por cada usuario registrado
        $data['confirmation_code']=RandomFacade::generateString(10, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');;
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'nombre_empresa' => $data['nombre_empresa'],
            'pais' => $data['pais'],
            'tipo' => $data['tipo'],
            'ciudad' => $data['ciudad'],
            'direccion' => $data['direccion'],
            'telefono' => $data['telefono'],
            'celular_e' => $data['celular_e'],
            'postal' => $data['postal'],
            'nit' => $data['nit'],
            'web' => $data['web'],
            'facebook' => $data['facebook'],
            'oferta' => $data['oferta'],
            'demanda' => $data['demanda'],
            'nombre_r' => $data['nombre_r'],
            'ci' => $data['ci'],
            'email_r' => $data['email_r'],
            'cargo' => $data['cargo'],
            'celular_r' => $data['celular_r'],
            'estado' => '0', //indica si el usuario esta activo(1) o inactivo(0)
            'tipo_user' => 'user',//indica que tipo de usuario es user=usuario normal admin=usuario administrador local=usuario designado por el admin
            'codigo' => $cod,
            'confirmation_code' => $data['confirmation_code'],
        ]);
     //para ctuar sobre los arrays "actv" y "srv" se crea una nueva variable
     //el formato de creacion de variable es: $variable= valor
        $actividad = $data['actv'];
        $servicios = $data['srv'];
       
     //las siguientes tres funciones introducen 3 nuevas filas en la tabla "actserv"
     //que corresponden a los campos actividad y servicios
     //este diseño se basa en que una empresa puede tener hasta 3 servicios y actividades
     //por lo tanto estas tres nuevas filas corresponden a una unca empresa 
     //el campo emp_id (llave foranea) esta asociado al id de una empresa    
        $actserv = Actserv::create([
            'emp_id' => $user->id,
            'actividad' => $actividad[0],
            'servicios' => $servicios[0],
        ]);
        $actserv = Actserv::create([
                'emp_id' => $user->id,
                'actividad' => $actividad[1],
                'servicios' => $servicios[1],
        ]);
        $actserv = Actserv::create([
            'emp_id' => $user->id,
            'actividad' => $actividad[2],
            'servicios' => $servicios[2],
        ]);
         

        //se asigna a cada usario registrado el tipo de usuario a user
        $user
        ->roles()
        ->attach(Role::where('name', 'user')->first());

        Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
            $message->subject('RAE Por favor confirme su correo');
            $message->to($data['email'],$data['password'],'codigo', $data['nombre_empresa']);
            
        });

        //$user->redirectTo();
        //return view('/emails/verificacion');
        return $user;   
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
    
        event(new Registered($user = $this->create($request->all())));
    
        //$this->guard()->login($user);
    
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

}//final----------------------------------------------------



