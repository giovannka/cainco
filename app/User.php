<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


use App\Notifications\CustomResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     //los siguientes campos corresponden a la tabla users 
    protected $fillable = [
        'email',
        'password', 
        'nombre_empresa',
        'name',
        'codigo',
        'pais',
        'ciudad',
        'direccion',
        'telefono',
        'celular_e',
        'postal',
        'nit',
        'web',
        'facebook',
        'logo',
        'estado',
        'tipo',
        'oferta',
        'demanda',
        'nombre_r',
        'ci',
        'email_r',
        'cargo',
        'celular_r',
        'confirmation_code',
        'confirmed',
        'tipo_user',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //esta funcion indica el tipo de relacion que se tiene con la tabla "actserv" 
    //"uno a muchos" (has many) esto quiere decir que la tabla users puede tener muchas relaciuones con la tabla actserv
    //de manera funcional una empresa(tabla users) puede tener varias actividades y servicios(tabla actserv) 
    //esta funcion permite establecer la relacion mediante la llave foranea indicada en la tabla actserv  
    public function actserv(){
        return $this->hasMany('Actserv');
     }


     //se establece la relacion con la tabla roles tomando en cuenta que un usuario puede ser admin o user o local
     //por lo tanto la relacion es pertenece a muchos (belongs to many)
     public function roles()
     {
         return $this
             ->belongsToMany('App\Role') //se hace referencia al modelo Role
             ->withTimestamps();
     }

     //funciones de validacion
     //estas funciones validan el tipo de usuario correspondiente y retornan true si lo es  
     public function authorizeRoles($roles)
     {
         if ($this->hasAnyRole($roles)) {
             return true;
         }
         abort(401, 'Esta acción no está autorizada.');
         return false;
     }
     
     public function hasAnyRole($roles)
     {
         if (is_array($roles)) {
             foreach ($roles as $role) {
                 if ($this->hasRole($role)) {
                     return true;
                 }
             }
         } else {
             if ($this->hasRole($roles)) {
                 return true;
             }
         }
         return false;
     }
     
     public function hasRole($role)
     {
         if ($this->roles()->where('name', $role)->first()) {
             return true;
         }
         return false;
     }

     public function userType($role)
     {
         if ($this->roles()->where('name', $role)->first()) {
             return true;
         }
         return false;
     }

     public function sendPasswordResetNotification($token)
     {
        $this->notify(new CustomResetPasswordNotification($token));
     }
}
