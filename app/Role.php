<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//este modelo esta asociado a la tabla roles
class Role extends Model
{
    //se establece la relacion de que los diferentes usduarios que existen pueden tener varios usuarios 
    //por lo tanto la relacion es pertenece a muchos (belongs to many)
    public function users()
    {
        return $this
            ->belongsToMany('App\User')
            ->withTimestamps();
    }
}
