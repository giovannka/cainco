<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//este modelo es funcional para la tabla "actserv"
class Actserv extends Model
{
    //se indica la tabla  ausar por este modelo
    protected $table = 'actserv';
    //se indican los campos a usar en la tabla "actserv"
    protected $fillable = [ 'actividad','servicios','emp_id'];

    //esta funcion permite la relacion con la tabla user
    //"pertenece a" (belong to) esto significa que esta tabla unicamente puede relacionarse con la tabla users
    //de manera funcional las actividades y servicios(tabla actserv) pertenecen a una unioca empresa(tabla users)
    //la relacion se da por la llave foranea "emp_id" que apunta al "id" de la tabla users
    public function user(){
        return $this->belongsTo('User');
     }
}
