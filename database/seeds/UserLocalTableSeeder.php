<?php


use Illuminate\Database\Seeder;
//se llaman a los modelos siguientes:
use App\Role;
use App\User;

//aqui se llena la primera fila de la db con datos para la cuenta administrador
class UserLocalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //se almacena en la variable $role_admin el resultado del query sql " Role::where('name', 'admin')" 
        //de la tabla Role
        $role_admin = Role::where('name', 'local')->first();
        
        //se crea un objeto $user de la tabla users (User) para llenar sus campos correspondientes
        $user = new User();

        $user->email = 'ingridpc@rae.com.bo';   //usuario de la cuenta
        $user->password = bcrypt('password'); //password de administrador
        $user->nombre_empresa = 'RAE1';
        $user->pais = 'Bolvia';
        $user->tipo = 'servicios';
        $user->ciudad = 'La Paz';
        $user->direccion = 'Parque Zenon Iturralde #120 Frente al Centro Boliviano Americano';
        $user->telefono = '';
        $user->celular_e = '76597505';
        $user->postal = '';
        $user->nit ='';
        $user->web = 'https://rae.com.bo/';
        $user->facebook = 'https://rae.com.bo/contacto.html#';
        $user->oferta = '';
        $user->demanda = '';
        $user->nombre_r = 'Ingrid Poma Cuentas';
        $user->ci = '0987456';
        $user->email_r = 'ingridpc@rae.com.bo';
        $user->cargo = 'SEO';
        $user->celular_r = '76597505';
        $user->estado = '1'; //indica si el usuario esta activo(1) o inactivo(0)
        $user->tipo_user = 'admin';//indica que tipo de usuario es user=usuario normal admin=usuario administrador local=usuario designado por el admin
        $user->codigo = '000001';
        $user->save(); //se llenan los campos de la tabla users
        $user->roles()->attach($role_admin); //agrega el campo admin en la tabla roles para este usuario
    }
}
