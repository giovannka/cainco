<?php

use Illuminate\Database\Seeder;
use App\Role;

//se llenan en la tabla roles los tipos de usuario 
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //usuario administrador
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrador';
        $role->save();

        //usuario local de mantenimiento
        $role = new Role();
        $role->name = 'local';
        $role->description = 'Cuenta local';
        $role->save();

        //usuario normal
        $role = new Role();
        $role->name = 'user';
        $role->description = 'Usuario';
        $role->save();
    }
}
