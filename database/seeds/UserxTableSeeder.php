<?php


use Illuminate\Database\Seeder;
//se llaman a los modelos siguientes:
use App\Role;
use App\User;

class UserxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //se almacena en la variable $role_admin el resultado del query sql " Role::where('name', 'admin')" 
        //de la tabla Role
        $role_admin = Role::where('name', 'admin')->first();
        
        //se crea un objeto $user de la tabla users (User) para llenar sus campos correspondientes
        $user = new User();

        $user->email = 'admin@rae.com.bo';   //usuario de la cuenta
        $user->password = bcrypt('password'); //password de administrador
        $user->nombre_empresa = 'RAE2';
        $user->pais = 'Bolvia';
        $user->tipo = 'servicios';
        $user->ciudad = 'La Paz';
        $user->direccion = 'Parque Zenon Iturralde #120 Frente al Centro Boliviano Americano';
        $user->telefono = '';
        $user->celular_e = '71516860';
        $user->postal = '';
        $user->nit ='6800618';
        $user->web = 'https://rae.com.bo/';
        $user->facebook = 'https://rae.com.bo/contacto.html#';
        $user->oferta = '';
        $user->demanda = '';
        $user->nombre_r = 'Jose Blas';
        $user->ci = '6800618';
        $user->email_r = 'bl455.dj@gmail.com';
        $user->cargo = 'SEO';
        $user->celular_r = '71516860';
        $user->estado = '1'; //indica si el usuario esta activo(1) o inactivo(0)
        $user->tipo_user = 'admin';//indica que tipo de usuario es user=usuario normal admin=usuario administrador local=usuario designado por el admin
        $user->codigo = '000003';
        $user->save(); //se llenan los campos de la tabla users
        $user->roles()->attach($role_admin); //agrega el campo admin en la tabla roles para este usuario
    }
}
