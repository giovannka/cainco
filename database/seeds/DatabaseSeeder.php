<?php

use Illuminate\Database\Seeder;

//una vez hecha las migraciones se ejcutan los seeder para el llenado inicial de tablas
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);
    
        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);

        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserLocalTableSeeder::class);

        $this->call(UserxTableSeeder::class);
    }
}
