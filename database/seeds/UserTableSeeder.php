<?php

use Illuminate\Database\Seeder;
//se llaman a los modelos siguientes:
use App\Role;
use App\User;

//aqui se llena la primera fila de la db con datos para la cuenta administrador
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //se almacena en la variable $role_admin el resultado del query sql " Role::where('name', 'admin')" 
        //de la tabla Role
        $role_admin = Role::where('name', 'admin')->first();
        
        //se crea un objeto $user de la tabla users (User) para llenar sus campos correspondientes
        $user = new User();

        $user->email = 'Info@rae.com.bo';   //usuario de la cuenta
        $user->password = bcrypt('password'); //password de administrador
        $user->nombre_empresa = 'RAE';
        $user->pais = 'Bolvia';
        $user->tipo = 'servicios';
        $user->ciudad = 'La Paz';
        $user->direccion = 'Parque Zenon Iturralde #120 Frente al Centro Boliviano Americano';
        $user->telefono = '';
        $user->celular_e = '78752442';
        $user->postal = '';
        $user->nit ='342916022';
        $user->web = 'https://rae.com.bo/';
        $user->facebook = 'https://rae.com.bo/contacto.html#';
        $user->oferta = '';
        $user->demanda = '';
        $user->nombre_r = 'Roberto Alba Monterrey';
        $user->ci = '1234567';
        $user->email_r = 'albamonterrey@gmail.com';
        $user->cargo = 'SEO';
        $user->celular_r = '78752442';
        $user->estado = '1'; //indica si el usuario esta activo(1) o inactivo(0)
        $user->tipo_user = 'admin';//indica que tipo de usuario es user=usuario normal admin=usuario administrador local=usuario designado por el admin
        $user->codigo = '000000';
        $user->save(); //se llenan los campos de la tabla users
        $user->roles()->attach($role_admin); //agrega el campo admin en la tabla roles para este usuario
    }
}
