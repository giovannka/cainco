<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//esta tabla almacena todos las actividades y servicios de una empresa
class CreateActservTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //se crean la tabla actserv
        //la llave foranea que apunta al "id" de la tabla "users"
        Schema::create('actserv', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('emp_id');
            $table->foreign('emp_id')->references('id')->on('users');
            $table->string('actividad')->nullable();
            $table->string('servicios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actserv');
    }
}
