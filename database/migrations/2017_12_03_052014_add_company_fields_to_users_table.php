<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//esta tabla es una extension de la tabla "users" los campos que estan aqui se juntan con los de la tbala users
//los valores "nullables" indican que esos campos pueden ser nulos 
//los campos unique indican que esos campos deben ser unicos y no pueden tener duplicados
class AddCompanyFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          // $table->string('name');
          // $table->string('email');
          // $table->string('password');
          
          $table->string('tipo_user'); //indica que tipo de usuario es
          $table->string('codigo');
          $table->string('nombre_empresa')->unique();
          $table->string('pais');
          $table->string('ciudad');
          $table->string('direccion')->nullable();
          $table->string('telefono')->nullable();
          $table->string('celular_e')->unique();
          $table->string('postal')->nullable();
          $table->string('nit')->nullable()->unique();
          $table->string('web')->nullable();
          $table->string('facebook')->nullable();
          $table->string('logo')->nullable()->default('usert.jpg');
          $table->string('estado');//indica si el usuario esta activo o no para participar de una rieda de negocio
          $table->string('tipo');
          $table->string('oferta');
          $table->string('demanda');
          // campos de responsable;
          $table->string('nombre_r');
          $table->string('ci')->unique();
          $table->string('email_r')->unique();
          $table->string('cargo');
          $table->string('celular_r')->unique();
          $table->boolean('confirmed')->default(0);
          $table->string('confirmation_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
