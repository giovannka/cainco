@extends('layouts.app')
@section('title', 'Business | Forum')
@section('content')
<div align="center">
     <img align="center" src="/images/xbforum.png" height="200" alt="">
</div>
<!-- esta seccion captura errores relacionados con los campos 
    <h1>errores</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif-->
<!-- esta seccion captura errores relacionados con los campos-->
<!-- cada campo del formulario recuerda el ultimo valor de su entrada para que el usuario no reingrese sus datos nuevamente ante un error-->
    @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li>Se detectaron errores
                                    <li>verifique los cuadros en rojo.
                                </ul>
                            </div>
                        @endif
    <div class="row background-form register-form form-control mt-9">
        <h4 class="title-form mb-3">FORMULARIO DE REGISTRO</h4>
        <form action="{{ url(config('adminlte.register_url', 'register')) }}" id="needs-validation" novalidate method="post" enctype=”multipart/form-data”>
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <p class="text-success">Los campos marcados con * son obligatorios</p>
                    <h4 class="title-form mb-3">Datos de la empresa</h4>                
                    <label for="nombre_empresa">* Nombre de la empresa</label>
                    <input type="text" class="form-control {{ $errors->has('nombre_empresa') ? ' is-invalid' : '' }}" id="nombre_empresa" name="nombre_empresa" value="{{ old('nombre_empresa') }}" placeholder="Nombre / Razon Social de la Empresa" required>
                    @if ($errors->has('nombre_empresa'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Posibles Errores:
                                    <li>Campo vacío
                                    <li>Nombre ya registrado
                                </ul>
                            </b> 
                        </div>
                    @endif
                    <p>
                    <p>
                    <label for="pais">* Pais</label>
                    <select  class="form-control {{ $errors->has('pais') ? ' is-invalid' : '' }}" name="pais" id="pais" >
                        @foreach ($countries as $country)
                          <option value="{{ $country }}" {{(old('pais') == $country?'selected':'')}} >{{ $country }}</option>
                        @endforeach
                    </select>
                     @if ($errors->has('pais'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Debe seleccionar un país
                                 </ul>
                            </b> 
                        </div>
                    @endif

                    <label for="tipo">* Tipo de empresa</label>
                    <select  class="form-control {{ $errors->has('tipo') ? ' is-invalid' : '' }}" name="tipo" id="tipo">
                        @foreach ($companyTypes as $companyType)
                          <option value="{{ $companyType }}" {{(old('tipo') == $companyType?'selected':'')}}>{{ $companyType }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tipo'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Debe seleccionar un tipo de empresa
                                 </ul>
                            </b> 
                        </div>
                    @endif

                    <p>
                    <label for="ciudad">* Ciudad</label>
                    <input type="text" class="form-control {{ $errors->has('ciudad') ? ' is-invalid' : '' }}" id="ciudad" name="ciudad" value="{{ old('ciudad') }}" placeholder="La Paz" required>
                     @if ($errors->has('ciudad'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Campo vacío
                                 </ul>
                            </b> 
                        </div>
                    @endif
                    
                    <label for="direccion">* Dirección</label>
                    <input type="text" class="form-control {{ $errors->has('direccion') ? ' is-invalid' : '' }}" id="direccion" name="direccion" value="{{ old('direccion') }}" placeholder="calle Bolivar #50 Zna Sopocachi" required>
                     @if ($errors->has('direccion'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Campo vacío
                                 </ul>
                            </b> 
                        </div>
                    @endif
                    
                    <label for="telefono">Teléfono de la empresa</label>
                    <input type="text" class="form-control {{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}" placeholder="Teléfono fijo">
                     @if ($errors->has('telefono'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Posibles Errores:
                                    <li>los datos introducidos deben ser números
                                 </ul>
                            </b> 
                        </div>
                    @endif

                    <label for="celular_e">* Celular de la empresa</label>
                    <input type="text" class="form-control {{ $errors->has('celular_e') ? ' is-invalid' : '' }}" id="celular_e" name="celular_e" value="{{ old('celular_e') }}" placeholder="71599999" required>
                     @if ($errors->has('celular_e'))                                 
                        <div class="invalid-feedback">
                         <b>
                                <ul>Posibles Errores:
                                    <li>Campo vacío
                                    <li>Celular ya registrado
                                    <li>los datos introducidos deben ser números
                                </ul>
                            </b> 
                        </div>
                    @endif
                    
                    <label for="web">Página web <span class="aclaration-text">(opcional)</span></label>
                    <input type="text" class="form-control" id="web" name="web" value="{{ old('web') }}" placeholder="empresa.com">
                    
                    <label for="facebook">Facebook</label>
                    <input type="text" class="form-control" id="facebook" name="facebook" value="{{ old('facebook') }}" placeholder="http://facebook.com/empresa">
                    
                    <label for="nit">NIT</label>
                    <input type="text" class="form-control {{ $errors->has('nit') ? ' is-invalid' : '' }}" id="nit" name="nit" value="{{ old('nit') }}" placeholder="123456">
                     @if ($errors->has('nit'))                                
                         <div class="invalid-feedback">
                            <b>
                                <ul>Posibles Errores:
                                    <li>NIT ya registrado
                                    <li>los datos introducidos deben ser números
                                 </ul>
                            </b> 
                         </div>
                    @endif

                    <label for="postal">Codigo postal</label>
                    <input type="text" class="form-control" id="postal" name="postal" value="{{ old('postal') }}" placeholder="">
                    
                    <label for="oferta">* Oferta</label>
                    <textarea name="oferta" id="oferta" rows="5" cols="66" class="form-control {{ $errors->has('oferta') ? ' is-invalid' : '' }}"  placeholder="porfavor describa la oferta de su empresa" required>{{{ old('oferta') }}}</textarea>
                     @if ($errors->has('oferta'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Campo vacío
                                 </ul>
                            </b>
                        </div>
                    @endif
                   
                    <label for="demanda">* Demanda</label>
                    <textarea name="demanda" id="demanda" rows="5" cols="66"  class="form-control {{ $errors->has('demanda') ? ' is-invalid' : '' }}" placeholder="porfavor describa la demanda de su empresa" required>{{{ old('demanda') }}}</textarea>
                     @if ($errors->has('demanda'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Campo vacío
                                 </ul>
                            </b>
                        </div>
                    @endif                   
                </div>

                <div class="col-sm-6 mb-3">
                    <!-- actv[] y srv[] son array que capturan los campos "select" -->
                    <h4>Seleccione la actividad y servicios que realiza su empresa</h4>
                    <label for="actv">* Actividad 1</label>
                    <select class="form-control {{ $errors->has('actv.0') ? ' is-invalid' : '' }}" name="actv[]" id="actv">
                        @foreach ($actividad as $activity)
                          <option value="{{ $activity }}" {{(old('actv.0') == $activity?'selected':'')}}>{{ $activity }}</option>
                        @endforeach
                    </select>
                     @if ($errors->has('actv.0'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Debe seleccionar una actividad 
                                 </ul>
                            </b>
                        </div>
                    @endif
                    <p>
                    <label for="srv">* Servicio 1</label>
                    <select class="form-control {{ $errors->has('srv.0') ? ' is-invalid' : '' }}" name="srv[]" id="srv">
                        @foreach ($servicio as $service)
                          <option value="{{ $service }}" {{(old('srv.0') == $service?'selected':'')}}>{{ $service }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('actv.0'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Debe seleccionar un servicio
                                 </ul>
                            </b>
                        </div>
                    @endif
                    <hr   height=2px >
                    <p>
                    <label for="actv">Actividad 2</label>
                    <select class="form-control" name="actv[]">
                        @foreach ($actividad as $activity)
                          <option value="{{ $activity }}" {{(old('actv.1') == $activity?'selected':'')}}>{{ $activity}}</option>
                        @endforeach
                    </select>
                    <p>
                    <label for="srv">Servicio 2</label>
                    <select class="form-control" name="srv[]" id="srv">
                        @foreach ($servicio as $service)
                          <option value="{{ $service }}" {{(old('srv.1') == $service?'selected':'')}}>{{ $service}}</option>
                        @endforeach
                    </select>
                    <p>
                    <label for="actv">Actividad 3</label>
                    <select class="form-control" name="actv[]" id="actv">
                        @foreach ($actividad as $activity)
                          <option value="{{ $activity }}" {{(old('actv.2') == $activity?'selected':'')}}>{{ $activity}}</option>
                        @endforeach
                    </select>
                    <p>
                    <label for="srv">Servicio 3</label>
                    <select class="form-control" name="srv[]" id="srv">
                        @foreach ($servicio as $service)
                          <option value="{{ $service }}" {{(old('srv.2') == $service?'selected':'')}}>{{ $service }}</option>
                        @endforeach
                    </select>
                    <p>
                   
                    
                   
                
                   <!----------------------------------------------------
                   <label for="logo">Logo de la empresa</label>
                   <p>
				    <input type="file" class="form-control-file {{ $errors->has('logo') ? ' is-invalid' : '' }}"  name="logo" id="logo" >
                     @if ($errors->has('logo'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Posibles Errores:
                                    <li>El logo solo puede ser: jpeg, png, bmp, gif, o svg
                                    <li>El logo supera 1Mb de tamaño
                                    <li>El archivo no es una imagen
                                 </ul>
                            </b>
                        </div>
                    @endif
                   -------------------------------------------------->
                    <h4 class="title-form mb-3">Datos del representante legal</h4>
                    <label for="nombre_r">* Nombre del Representante legal</label>
                    <input type="text" name="nombre_r" id="nombre_r" class="form-control {{ $errors->has('nombre_r') ? ' is-invalid' : '' }}" value="{{ old('nombre_r') }}" placeholder="Jesus Torrez" required>
                     @if ($errors->has('nombre_r'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Errores:
                                    <li>Campo vacío
                                 </ul>
                            </b>
                        </div>
                    @endif
                    
                    <label for="ci">* CI</label>
                    <input type="text" class="form-control {{ $errors->has('ci') ? ' is-invalid' : '' }}" id="ci" name="ci" value="{{ old('ci') }}" placeholder="123456" required>
                     @if ($errors->has('ci'))                                 
                        <div class="invalid-feedback">
                            <b>
                                <ul>Posibles Errores:
                                    <li>Campo vacío
                                    <li>CI ya registrado
                                    <li>los datos introducidos deben ser números
                                </ul>
                            </b> 
                        </div>
                    @endif
                    
                    <label for="email_r">* Email</label>
                    <input type="text" name="email_r" id="email_r" class="form-control {{ $errors->has('email_r') ? ' is-invalid' : '' }}" value="{{ old('email_r') }}" placeholder="jesusadmin@empresa.com" required>
                     @if ($errors->has('email_r'))                                 
                        <div class="invalid-feedback">
                        <b>
                                <ul>Posibles Errores:
                                    <li>Campo vacío
                                    <li>Email ya registrado
                                    <li>La dirección no tiene la forma ejemplo@mail.com
                                </ul>
                            </b>
                        </div>
                    @endif
                    
                    <label for="cargo">* Cargo</label>
                    <input type="text" name="cargo" id="cargo" class="form-control {{ $errors->has('cargo') ? ' is-invalid' : '' }}" value="{{ old('cargo') }}" placeholder="Director Gral" required>
                     @if ($errors->has('cargo'))                                 
                        <div class="invalid-feedback">
                        <b>
                                <ul>Errores:
                                    <li>Campo vacío
                                 </ul>
                            </b>
                        </div>
                    @endif
                    
                    <label for="celular_r">* Celular</label>
                    <input type="text" name="celular_r" id="celular_r" class="form-control {{ $errors->has('celular_r') ? ' is-invalid' : '' }}" value="{{ old('celular_r') }}" placeholder="71234567" required>
                     @if ($errors->has('celular_r'))                                 
                        <div class="invalid-feedback">
                        <b>
                                <ul>Posibles Errores:
                                    <li>Campo vacío
                                    <li>Celular ya registrado
                                    <li>los datos introducidos deben ser números
                                </ul>
                            </b>
                        </div>
                    @endif

                    <div class="register-fields-container">
                    <p>
                        <h4>Los siguientes datos seran usados para <b>iniciar sesion</b> en su cuenta.</h4>
                        <div class="col-sm-12 mb-3">                            
                            <label for="email"><h4><p class="text-danger"><b>* Email de la empresa</b></p><h4></label>
                            <p class="text-danger"><b>Este email sera usado para la verificación, acceso y restablecimiento de su cuenta no la pierda!</b></p>
                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="email@empresa.com" required>
                             @if ($errors->has('email'))                                 
                                <div class="invalid-feedback">
                                <b>
                                    <ul>Posibles Errores:
                                        <li>Campo vacío
                                        <li>Email ya registrado
                                        <li>La dirección no tiene la forma ejemplo@mail.com
                                    </ul>
                                </b>
                                </div>
                            @endif
                        </div>

                        <div class="col-sm-12 mb-3">
                            <label for="password">* Contraseña</label>
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ trans('adminlte::adminlte.password') }}" required>
                             @if ($errors->has('password'))                                 
                                <div class="invalid-feedback">
                                    <b>
                                        <ul>Errores:
                                            <li>La contraseña debe contener minimo 6 caracteres
                                        </ul>
                                    </b>
                                </div>
                            @endif
                        </div>

                        <div class="col-sm-12 mb-3">
                            <label for="password_confirmation">* Repita su contraseña</label>
                            <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="{{ trans('adminlte::adminlte.retype_password') }}" required>
                             @if ($errors->has('password'))                                 
                                <div class="invalid-feedback">
                                    <b>
                                         <ul>Errores:
                                         <li>Campo vacío
                                        </ul>
                                     </b>
                                </div>
                            @endif
                        </div>
                     </div> 
                     <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-success btn-block btn-flat">Registrar</button>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li>Se detectaron errores
                                    <li>verifique los cuadros en rojo.
                                </ul>
                            </div>
                        @endif
                    </div> 
                </div>
            </div>
        </form>
    </div>
    <script>
    // Disabling form submissions if there are invalid fields
    (function() {
    'use strict';

    window.addEventListener('load', function() {
        var form = document.getElementById('needs-validation');
        form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        form.classList.add('was-validated');
        }, false);
    }, false);
    })();
    </script>
@endsection
