@extends('layouts.app')
@section('title', 'Business | Forum')
@section('content')
    <div class="container">
        <div class="row align-items-center" style="height:85vh;">
            <div class="container-fluid">
                    <div class="row align-items-center login-form">
                            
                            <div class="col logo-divider" align="center" >
                                <img src="images/xbforum.png" width="200"  align="center" alt="Logo-Cainco">
                                <h2><i class="fa fa-users"></i> Iniciar sesion</h2>
                                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">Email</label>
                                        <div class="col-md-4">
                                            <input id="email"  type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                     
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Contraseña</label>

                                        <div class="col-md-4">
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-success">
                                                Iniciar sesion
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button onclick="location.href='{{ url('registro') }}'" type="submit" class="btn btn-success">
                                                Registrarse
                                            </button>
                                            <p>
                                            <a style="color:#F9AB5B;" class="btn btn-link" href="{{ route('password.request') }}">
                                                Olvidaste tu password?
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                    <ul>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Direccion: calle Cañada Strongest Nro 1620 esquina Otero de la Vega - San Pedro.
                                            Telefono:(591-2)2129891,2121764
                                            765 97505 - 705 31667
                                        </li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i> (591) 78752442</li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i> info@rae.com.bo</li>
                                    </ul>
                                    </div>
                                </form>
                            </div>
                        </div>
            </div>
        </div>
    </div>
@endsection