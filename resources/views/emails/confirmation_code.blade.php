@extends('layouts.app')
@section('title', 'Business | Forum')
@section('content')
    <div class="container">
        <div class="row align-items-center" style="height:85vh;">
            <div class="container-fluid">
                    <div class="row align-items-center login-form">                            
                            <div class="col logo-divider" align="center" >
                                <img src="images/xbforum.png" width="200"  align="center" alt="Logo-Cainco">
                                <h2>Hola {{$nombre_empresa}}, gracias por registrarse en <strong>Bussines Forum</strong> !</h2>
                                    <b>
                                        <ul>podra acceder a su cuenta con los siguientes datos:
                                        <li><h4>Email: {{$email}}</h4>
                                        <li><h4>contraseña: {{$password}}</h4>
                                        </ul>
                                    </b>
                                    <p>Por favor confirme su correo electrónico.</p>
                                    <p>Para ello simplemente haga click en el siguiente enlace:</p>

                                    <a href="{{ url('/emails/' . $confirmation_code) }}">
                                        Clic para confirmar tu email
                                    </a>
                            </div>
                    </div>
            </div>
        </div>
    </div>
@endsection



    
