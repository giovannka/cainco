@extends('layouts.app')
@section('title', 'Business | Forum')
@section('content')
    <div class="container">
        <div class="row align-items-center" style="height:85vh;">
            <div class="container-fluid">
                    <div class="row align-items-center login-form">
                            
                            <div class="col logo-divider" align="center" >
                                <img src="images/xbforum.png" width="200"  align="center" alt="Logo-Cainco">
                                <h4>Se ha registrado exitosamente. Se le {{ $email }} enviará un correo electrónico a  para su verificación.
                            </div>
                        </div>
            </div>
        </div>
    </div>
@endsection

